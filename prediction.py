import pandas as pd
import sqlite3
import requests
import numpy as np
import matplotlib.pyplot as plt
from arch import arch_model
from config import settings
from data import SQLRepository
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

ticker = "MTNOY"
output_size = "full"
data_type = "json"
url = (
    f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={ticker}&outputsize={output_size}&dtattype={data_type}&apikey={settings.alpha_api_key}")
print("url type:", type(url))

response = requests.get(url=url)
print("response type:", type(response))

response_code = response.status_code
print("code type:", type(response_code))
response_code

response_data = response.json()
stock_data = response_data["Time Series (Daily)"]

df_mtnoy = pd.DataFrame.from_dict(stock_data, orient="index",dtype=float)
df_mtnoy.index = pd.to_datetime(df_mtnoy.index)
df_mtnoy.index.name = "date"
df_mtnoy.columns = [c.split(". ")[1] for c in df_mtnoy.columns]
print("df_mtnoy shape:", df_mtnoy.shape)

print(df_mtnoy.info())
df_mtnoy.head(10)

connection = sqlite3.connect(database=settings.db_name, check_same_thread=False)

from data import SQLRepository

repo = SQLRepository(connection = connection)
response = repo.insert_table(table_name=ticker, records=df_mtnoy,if_exists="replace")

sql = "SELECT * FROM 'MTNOY.BSE'"
df_mtnoy_read = pd.read_sql(sql=sql,con=connection,parse_dates=["date"], index_col="date")
df_mtnoy_read.head()

def wrangle(ticker, n_observations):
    df = repo.read_table(table_name=ticker,limit=n_observations+1)
    df.sort_index(ascending=True, inplace=True)
    df["return"] = df["close"].pct_change()*100
    return df["return"].dropna()

y_mtnoy = wrangle(ticker=ticker, n_observations=2500)
y_mtnoy.head()

mtnoy_daily_volatality = y_mtnoy.std()
print("MTN daily volatality: ", mtnoy_daily_volatality)

mtnoy_annual_volatality = mtnoy_daily_volatality*np.sqrt(252)
print("MTN annual volatality: ", mtnoy_annual_volatality)

fig, ax =plt.subplots(figsize=(15,6))
y_mtnoy.plot(ax=ax, label="daily return")

plt.legend()

fig, ax =plt.subplots(figsize=(15,6))
plot_acf(y_mtnoy**2, ax=ax)
plt.xlabel("lag[days]")
plt.ylabel("correlation coeff")
plt.title("ACf")

fig, ax =plt.subplots(figsize=(15,6))
plot_pacf(y_mtnoy**2, ax=ax)
plt.xlabel("lag[days]")
plt.ylabel("correlation coeff")
plt.title("PACf")

cutoff_test = int(len(y_mtnoy)*0.8)
y_mtnoy_train = y_mtnoy[:cutoff_test]

#GARCH Model
model = arch_model(y_mtnoy_train,p=1,q=1,rescale=False).fit(disp=0)
print(model.summary)